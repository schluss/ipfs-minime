'use strict';

// Load modules ---------------------------------------

var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

var sourcemaps = require('gulp-sourcemaps');
var log = require('gulplog');
var terser = require('gulp-terser');


// Configuration settings ---------------------------------------

var config = {
	
	base : { 
		src : './src/',
		dist : './dist/'
	},
	
	tersercfg : { mangle: { toplevel: true } }
};

// Tasks ---------------------------------------

gulp.task('default', function(){
  
	gulp.src(config.base.src + "ipfs-minime.js", {read: true}) 
	.pipe(gulp.dest(config.base.dist));
  
  var b = browserify({
    entries: config.base.src + 'ipfs-minime.js',
    debug: false
  });	
	
  return b.bundle()
    .pipe(source('ipfs-minime.min.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
        // Add transformation tasks to the pipeline here.
        .pipe(terser(config.tersercfg))
        .on('error', log.error)
   .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.base.src)) // copy to both src and dist to be sure
    .pipe(gulp.dest(config.base.dist));
});