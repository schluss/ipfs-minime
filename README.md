# IPFS MiniMe
Inspired by the IPFS Mini Javascript library, created by SilentCicero; found here: https://github.com/SilentCicero/ipfs-mini

## Changes 
- Made the functions async with Promises
- Removed the 'window.' reference, so we can use this library in WebWorkers 
- Build automation with gulp	