var _ipfs = {
	
	provider : null,
	requestBase : null,
	
	setProvider : function(provider)
	{
		if (typeof provider !== 'object') { throw new Error('[ifpsjs] provider must be type Object, got ' + typeof provider + '.'); }
	
			var data = _ipfs.provider = Object.assign({
				host: '127.0.0.1',
				pinning: true,
				port: '5001',
				protocol: 'http',
				base: '/api/v0' }, provider || {});
	  requestBase = String(data.protocol +'://'+ data.host + ':' + data.port + data.base);
	},
	
	sendAsync : async function(opts) {

		return new Promise(function(resolve, reject){ 
	
		  var request = new XMLHttpRequest(); //request.init(); //new XMLHttpRequest(); // eslint-disable-line
		  var options = opts || {};

		  request.onreadystatechange = function() {
			if (request.readyState === 4 && request.timeout !== 1) {

			  if (request.status !== 200) {
				reject(new Error('[ipfs-mini] status '+request.status+': '+ request.responseText));
			  } else {
				try {
				  resolve(options.jsonParse ? JSON.parse(request.responseText) : request.responseText);
				} catch (jsonError) {
				  reject(new Error('[ipfs-mini] while parsing data: '+ String(request.responseText) +', error: '+String(jsonError)+' with provider: ' + requestBase));
				}
			  }
			}
		  };

		  var pinningURI = _ipfs.provider.pinning && opts.uri === '/add' ? '?pin=true' : '';

		  if (options.payload) {
			request.open('POST', requestBase + opts.uri + pinningURI);
		  } else {
			request.open('GET', requestBase + opts.uri + pinningURI);
		  }

		  if (options.accept) {
			request.setRequestHeader('accept', options.accept);
		  }

		  if (options.payload && options.boundary) {
			request.setRequestHeader('Content-Type', 'multipart/form-data; boundary='+ options.boundary);
			request.send(options.payload);
		  } else {
			request.send();
		  }
		  
		});
	},	
	
	createBoundary : function(data) {
	  while (true) {
		var boundary = '----IPFSMini'+Math.random() * 100000+'.'+Math.random() * 100000;
		if (data.indexOf(boundary) === -1) {
		  return boundary;
		}
	  }
	},
	
	// heette: addData, exposed als add
	add : function(input) {
		
		return new Promise(async function(resolve, reject){
		
			try 
			{
		
			  var data = ((typeof input === 'object' && input.isBuffer) ? input.toString('binary') : input);
			  var boundary = _ipfs.createBoundary(data);
			  var payload = '--'+boundary+"\r\nContent-Disposition: form-data; name=\"path\"\r\nContent-Type: application/octet-stream\r\n\r\n"+data+"\r\n--"+boundary+"--";

			  //const addCallback = (err, result) => callback(err, (!err ? result.Hash : null));
			  let result = await _ipfs.sendAsync({
				jsonParse: true,
				accept: 'application/json',
				uri: '/add',
				payload, boundary,
			  });
			  console.log('result:' + result);
			  resolve(result.Hash);
		  
			}
			catch(err){
				console.log('err:');
				console.log(err);
				reject(err);
			};
		  
		});
	},

	addJSON : async function(jsonData) {
	  return new Promise(async function(resolve, reject){
		let result = await _ipfs.add(JSON.stringify(jsonData));
		
		resolve(result);
		
	  });
	},	
	
	stat : async function(ipfsHash) {
	  return new Promise(async function(resolve, reject){
		  try
		  {
			  let result = await _ipfs.sendAsync({ jsonParse: true, uri: '/object/stat/'+ipfsHash });
				resolve(result);
			  
		  }
		  catch(e)
		  {
			  console.log(e);
			  reject(e);
		  }
		});
	},
	
	cat : function(ipfsHash) {
	  return new Promise(async function(resolve, reject){
		let result = await _ipfs.sendAsync({ uri: '/cat/' + ipfsHash });
		resolve(result);
	  
	  });
	},
	
	catJSON : function(ipfsHash) {
		return new Promise(async function(resolve, reject){
			try {
				let result = await _ipfs.cat(ipfsHash);
				
				resolve(JSON.parse(result));
			} 
			catch (e) {
				reject(e);
			}
		});
	}
};

module.exports = _ipfs;